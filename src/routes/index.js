import React from "react";
import { Route, Switch } from "react-router-dom";
import FilterDialogSport from "components/shared/FilterDialog/FilterDialogSport";
import Home from "../components/Home/Home";
import NewEvent from "../components/newEvent/NewEvent";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/Home" exact component={Home} />
      <Route path="/Filter" exact component={FilterDialogSport} />
      <Route path="/createEvent" exact component={NewEvent} />
    </Switch>
  );
};

export default Routes;
