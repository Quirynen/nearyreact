import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import clsx from "clsx";
import IconButton from "@material-ui/core/IconButton";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import FormControl from "@material-ui/core/FormControl";
import ContactMailOutlinedIcon from "@material-ui/icons/ContactMailOutlined";
import VisibilityOffOutlinedIcon from "@material-ui/icons/VisibilityOffOutlined";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import nearyImg from "../../images/nearyFeedImg.png";
import { signUpUser, ReinitState } from "../../actions/apiFetch/user";
import { connect } from "react-redux";
import SnackbarComp from "./SnackbarComp";

const useStyles = makeStyles(theme => ({
  container: {
    textAlign: "center"
  },
  margin: {
    margin: theme.spacing(1)
  },
  withoutLabel: {
    marginTop: theme.spacing(3)
  },
  textField: {
    width: "80%"
  },
  btnCancel: {
    marginRight: 15,
    width: "30%"
  },
  btnContainer: {
    textAlign: "center",
    marginBottom: 25
  },
  btnRegister: {
    width: "30%"
  },
  divider: {
    marginBottom: 35,
    marginTop: 35
  },
  imgIcon: {
    height: 150,
    margin: "auto",
    borderRadius: "50%"
  }
}));
const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  isSignUpSuccess: state.user.isSignUpSuccess,
  error: state.user.error
});

const mapDispatchToProps = {
  signUpUser,
  ReinitState
};
function RegisterDialog(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    username: "",
    password: "",
    mail: "",
    showPassword: false
  });

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };
  const handleClose = () => {
    props.close();
    props.ReinitState();
  };
  const handleRegister = () => {
    props.signUpUser(values.username, values.password, values.mail);
  };
  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  if (props.isSignUpSuccess) {
    handleClose();
  }
  return (
    <div className={classes.container}>
      {props.error.length > 0 ? (
        <SnackbarComp message={props.error} type="error" />
      ) : (
        ""
      )}
      {props.isSignUpSuccess ? (
        <SnackbarComp message="Successfully registered" type="success" />
      ) : (
        ""
      )}
      <center>
        <img src={nearyImg} alt="icon" className={classes.imgIcon} />
      </center>
      <Divider light className={classes.divider} />
      <div className={classes.contenttext}>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Username
          </InputLabel>
          <OutlinedInput
            id="username"
            value={values.username}
            onChange={handleChange("username")}
            endAdornment={
              <InputAdornment position="start">
                <AccountCircleOutlinedIcon />
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Password
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? "text" : "password"}
            value={values.password}
            onChange={handleChange("password")}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? (
                    <VisibilityOutlinedIcon />
                  ) : (
                    <VisibilityOffOutlinedIcon />
                  )}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="mail">Mail</InputLabel>
          <OutlinedInput
            id="mail"
            value={values.mail}
            onChange={handleChange("mail")}
            endAdornment={
              <InputAdornment position="start">
                <ContactMailOutlinedIcon />
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
      </div>
      <Divider light className={classes.divider} />
      <div className={classes.btnContainer}>
        <Button
          onClick={handleClose}
          variant="contained"
          className={classes.btnCancel}
        >
          Cancel
        </Button>
        <Button
          onClick={handleRegister}
          variant="contained"
          color="primary"
          className={classes.btnRegister}
        >
          Register
        </Button>
      </div>
    </div>
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(RegisterDialog);
