import React from "react";
import {
  SportsBasketballOutlined,
  SportsHandballOutlined,
  SportsTennisOutlined,
  SportsMmaOutlined,
  SportsRugbyOutlined,
  SportsSoccerOutlined,
  SportsGolfOutlined,
  SportsBaseballOutlined
} from "@material-ui/icons";
import SportsIcon from "@material-ui/icons/Sports";

export const sports = [
  {
    icon: <SportsSoccerOutlined />,
    text: "Football"
  },
  {
    icon: <SportsTennisOutlined />,
    text: "Tennis"
  },
  {
    icon: <SportsIcon />,
    text: "Badminton"
  },
  {
    icon: <SportsHandballOutlined />,
    text: "Handball"
  },
  {
    icon: <SportsBasketballOutlined />,
    text: "Basket"
  },
  {
    icon: <SportsBaseballOutlined />,
    text: "Baseball"
  },
  {
    icon: <SportsMmaOutlined />,
    text: "Mma"
  },
  {
    icon: <SportsRugbyOutlined />,
    text: "Rugby"
  },
  {
    icon: <SportsGolfOutlined />,
    text: "Golf"
  }
];
export default {
  sports
};
