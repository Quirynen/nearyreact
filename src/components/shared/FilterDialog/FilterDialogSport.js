import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Dialog,
  Divider,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Slide,
  Slider
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import CheckboxList from "./CheckBoxList";
import { Link } from "react-router-dom";
const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  divider: {
    margin: theme.spacing(8)
  },
  sliderStyle: {
    width: 500
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FilterDialogSport(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [value, setValue] = React.useState(
    localStorage.getItem("km") !== null
      ? parseInt(localStorage.getItem("km"))
      : 20
  );

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleClose = () => {
    localStorage.setItem("km", value);
    setOpen(false);
    if (props.close) {
      props.close();
    }
  };

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Link to="/Home">
              <IconButton
                style={{ textDecoration: "none", color: "white" }}
                edge="start"
                color="inherit"
                onClick={handleClose}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
            </Link>
            <Typography variant="h6" className={classes.title}>
              Filter Sports
            </Typography>
            <Link to="/Home">
              <Button
                autoFocus
                style={{ textDecoration: "none", color: "white" }}
                onClick={handleClose}
              >
                Save
              </Button>
            </Link>
          </Toolbar>
        </AppBar>
        <center>
          <CheckboxList />
          <Divider className={classes.divider} />
          <Typography id="discrete-slider" gutterBottom>
            {value} Km around :
          </Typography>
          <Slider
            value={typeof value === "number" ? value : 0}
            onChange={handleSliderChange}
            className={classes.sliderStyle}
          />
          <Divider className={classes.divider} />
        </center>
      </Dialog>
    </div>
  );
}
