import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import { sports } from "components/shared/SportPossibilities";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360
  }
}));

export default function CheckboxList() {
  const classes = useStyles();

  const sportstext = sports.map(e => e.text);

  const localStorageSports =
    localStorage.getItem("sports") !== null
      ? JSON.parse(localStorage.getItem("sports"))
      : sportstext;
  const [checked, setChecked] = React.useState(localStorageSports);

  const handleToggle = value => () => {
    const currentIndex = checked.indexOf(value.text);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value.text);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
    localStorage.removeItem("sports");
    localStorage.setItem("sports", JSON.stringify(newChecked));
  };
  return (
    <List className={classes.root}>
      {sports.map(item => {
        const labelId = `checkbox-list-label-${item}`;
        return (
          <ListItem
            key={item.text}
            role={undefined}
            dense
            button
            onClick={handleToggle(item)}
          >
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={checked.indexOf(item.text) !== -1}
                tabIndex={-1}
                disableRipple
                color="primary"
                inputProps={{ "aria-labelledby": labelId }}
              />
            </ListItemIcon>
            <ListItemText primary={item.text} />
            <ListItemSecondaryAction>
              <IconButton edge="end">{item.icon}</IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List>
  );
}
