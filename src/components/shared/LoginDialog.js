import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import {
  loginUser,
  storeUserInfos,
  ReinitState
} from "../../actions/apiFetch/user";
import nearyImg from "../../images/nearyFeedImg.png";
import Divider from "@material-ui/core/Divider";
import SocialButton from "./SocialButton";
import SnackbarComp from "components/shared/SnackbarComp";

const useStyles = makeStyles => ({
  container: {
    textAlign: "center"
  },
  icon: {
    marginRight: 15
  },
  contenttext: {
    marginBottom: 25
  },
  socialLogins: {
    textAlign: "center"
  },
  imgIcon: {
    height: 80,
    margin: "auto"
  },
  btnCancel: {
    marginRight: 15,
    width: "30%"
  },
  btnContainer: {
    textAlign: "center",
    marginBottom: 25
  },
  btnLogin: {
    width: "30%"
  },
  divider: {
    marginBottom: 15
  },
  btnGoogle: {
    verticalAlign: "middle",
    marginRight: 15
  },
  btnFacebook: {
    verticalAlign: "middle",
    textAlign: "right",
    float: "right"
  }
});
class LoginDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      setOpen: false,
      username: "",
      password: ""
    };
  }
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  };
  // Quand login réussit au niveau front-end, on valide côté serveur
  handleLoginGoogle = user => {
    var token = user._token.idToken;
    fetch("http://localhost:8051/api/validateGoogleToken", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(token)
    })
      .then(response => response.json())
      .then(json => {
        console.log(json);
        this.props.storeUserInfos(json);
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleLoginFacebook = user => {};
  handleLoginApp = () => {
    this.props.loginUser(this.state.username, this.state.password);
    if (this.props.isLoggedIn) {
      this.handleClose();
    }
  };
  handleSocialLoginFailure = err => {
    console.error(err);
  };
  handleClickOpen = () => {
    this.setState({ open: true, setOpen: true });
  };

  handleClose = () => {
    this.props.close();
    this.props.ReinitState();
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.props.error.length > 0 ? (
          <SnackbarComp message={this.props.error} type="error" />
        ) : (
          ""
        )}
        {this.props.isLoggedIn ? (
          <SnackbarComp message="Login Successfull" type="success" />
        ) : (
          ""
        )}
        <center>
          <img src={nearyImg} alt="icon" className={classes.imgIcon} />
        </center>
        <form className={classes.form} noValidate>
          <div className={classes.contenttext}>
            <TextField
              id="username"
              required
              label="Username"
              fullWidth
              margin="normal"
              onChange={this.handleChange}
              autoComplete="no"
            />
            <TextField
              margin="normal"
              required
              id="password"
              label="Password"
              type="password"
              onChange={this.handleChange}
              fullWidth
              autoComplete="no"
            />
          </div>
          <div className={classes.btnContainer}>
            <Button
              onClick={this.handleClose}
              variant="contained"
              className={classes.btnCancel}
            >
              Cancel
            </Button>
            <Button
              onClick={this.handleLoginApp}
              variant="contained"
              color="primary"
              className={classes.btnLogin}
            >
              Login
            </Button>
          </div>
        </form>
        <Divider light className={classes.divider} />
        <div className={classes.socialLogins}>
          <div>
            <img
              width="20px"
              alt="Google Logo"
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"
              className={classes.btnGoogle}
            />
            <SocialButton
              className={classes.btnGoogle}
              provider="google"
              appId="347183167777-qv9f1nkhoahshu1n743upn05s3lrtsll.apps.googleusercontent.com"
              onLoginSuccess={this.handleLoginGoogle}
              onLoginFailure={this.handleSocialLoginFailure}
            >
              Login with google
            </SocialButton>
          </div>
          <div>
            <img
              width="20px"
              alt="Facebook Logo"
              src="https://en.facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png"
              className={classes.btnGoogle}
            />
            <SocialButton
              className={classes.btnFacebook}
              provider="facebook"
              appId="2782157128461809"
              onLoginSuccess={this.handleLoginFacebook}
              onLoginFailure={this.handleSocialLoginFailure}
            >
              Login With Facebook
            </SocialButton>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn,
  error: state.user.error,
  token: state.user.token,
  user: state.user
});

const mapDispatchToProps = {
  loginUser,
  storeUserInfos,
  ReinitState
};
const redirect = connect(mapStateToProps, mapDispatchToProps)(LoginDialog);

export default withStyles(useStyles)(redirect);
