import React, { Component } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import FilterListIcon from "@material-ui/icons/FilterList";
import {
  Drawer,
  AppBar,
  Tooltip,
  Toolbar,
  List,
  CssBaseline,
  IconButton,
  Menu,
  MenuItem,
  Divider,
  ListItemText,
  ListItemIcon,
  ListItem,
  Typography,
  LinearProgress
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from "react-router-dom";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Routes from "routes";
import nearyImg from "../../images/nearyFeedImg.png";
import AccountCircle from "@material-ui/icons/AccountCircle";
import HomeTwoToneIcon from "@material-ui/icons/HomeTwoTone";
import AccountCircleTwoToneIcon from "@material-ui/icons/AccountCircleTwoTone";
import EventAvailableTwoToneIcon from "@material-ui/icons/EventAvailableTwoTone";
import InsertInvitationTwoToneIcon from "@material-ui/icons/InsertInvitationTwoTone";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import LoginTabs from "./userInteractions";

const drawerWidth = 240;

const useStyles = makeStyles => ({
  root: {
    display: "flex"
  },
  appBar: {
    zIndex: makeStyles.zIndex.drawer + 1,
    transition: makeStyles.transitions.create(["width", "margin"], {
      easing: makeStyles.transitions.easing.sharp,
      duration: makeStyles.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: makeStyles.transitions.create(["width", "margin"], {
      easing: makeStyles.transitions.easing.sharp,
      duration: makeStyles.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: makeStyles.transitions.create("width", {
      easing: makeStyles.transitions.easing.sharp,
      duration: makeStyles.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: makeStyles.transitions.create("width", {
      easing: makeStyles.transitions.easing.sharp,
      duration: makeStyles.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: makeStyles.spacing(7) + 1,
    [makeStyles.breakpoints.up("sm")]: {
      width: makeStyles.spacing(9) + 1
    }
  },
  alignRight: {
    textAlign: "right"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: makeStyles.spacing(0, 1),
    ...makeStyles.mixins.toolbar
  },
  imgIcon: {
    height: 45,
    margin: "auto"
  },
  title: {
    flexGrow: 1,
    textAlign: "center"
  },
  content: {
    flexGrow: 1,
    padding: makeStyles.spacing(3)
  }
});

class MiniDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      setOpen: false,
      anchorEl: null,
      setAnchorEl: null,
      accountOpen: false,
      openLoginDialog: false,
      openSortSportDialog: false
    };
  }

  handleDrawerOpen = () => {
    this.setState({ setOpen: true, open: true });
  };

  handleDrawerClose = () => {
    this.setState({ setOpen: false, open: false });
  };
  handleMenu = event => {
    this.setState({
      setAnchorEl: event.currentTarget,
      anchorEl: event.currentTarget,
      accountOpen: true
    });
  };
  handleSortFilter = () => {
    this.setState({ openSortSportDialog: true });
  };
  handleClose = () => {
    this.setState({ setAnchorEl: null, anchorEl: null, accountOpen: false });
    this.setState({ openSortSportDialog: false });
  };

  render() {
    const { classes } = this.props;
    const msgLogin = this.props.isLoggedIn ? "Profil" : "Login";
    const { isLoggedIn } = this.props;
    const drawerLeft = [
      {
        icon: <HomeTwoToneIcon />,
        text: "Home"
      },
      {
        icon: <AccountCircleTwoToneIcon />,
        text: "My Profil"
      },
      {
        icon: <EventAvailableTwoToneIcon />,
        text: "My Events"
      },
      {
        icon: <InsertInvitationTwoToneIcon />,
        text: "Invitations"
      }
    ];
    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: this.state.open
          })}
        >
          {this.props.isLoading ? <LinearProgress /> : ""}
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: this.state.open
              })}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              NearySport
            </Typography>
            <div className={classes.alignRight}>
              <Link to="/Filter">
                <IconButton
                  style={{ textDecoration: "none", color: "white" }}
                  aria-label="Filter"
                  onClick={this.handleSortFilter}
                  color="inherit"
                >
                  <Tooltip title="Filter sports">
                    <FilterListIcon />
                  </Tooltip>
                </IconButton>
              </Link>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={this.handleMenu}
                color="inherit"
              >
                <Tooltip title={msgLogin}>
                  <AccountCircle />
                </Tooltip>
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={this.state.anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                open={this.state.accountOpen}
                onClose={this.handleClose}
              >
                {isLoggedIn ? (
                  <div>
                    <MenuItem onClick={this.handleClose}>My account</MenuItem>
                    <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                  </div>
                ) : (
                  <div>
                    <LoginTabs close={this.handleClose} />
                  </div>
                )}
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open
            })
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <img src={nearyImg} alt="icon" className={classes.imgIcon} />
            <IconButton onClick={this.handleDrawerClose}>
              {makeStyles.direction === "rtl" ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          <List>
            {drawerLeft.map(item => (
              <ListItem
                button
                key={item.text}
                component={Link}
                to={"/" + item.text}
              >
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={item.text} />
              </ListItem>
            ))}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Routes />
        </main>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn,
  isLoading: state.common.isLoading,
  token: state.user.token,
  user: state.user
});

const mapDispatchToProps = {};
const redirect = connect(mapStateToProps, mapDispatchToProps)(MiniDrawer);

export default withStyles(useStyles)(redirect);
