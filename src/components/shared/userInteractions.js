import React from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import LoginDialog from "./LoginDialog";
import RegisterDialog from "./RegisterDialog";
import Button from "@material-ui/core/Button";
import OpenInNewTwoToneIcon from "@material-ui/icons/OpenInNewTwoTone";
import Dialog from "@material-ui/core/Dialog";
import { makeStyles } from "@material-ui/core/styles";
import { ReinitState } from "../../actions/apiFetch/user";
import { connect } from "react-redux";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};
function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={event => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500
  },
  icon: {
    marginRight: 15
  }
}));

const mapDispatchToProps = {
  ReinitState
};
function LoginTabs(props) {
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const handleClickOpen = () => {
    setOpen(true);
    props.close();
  };

  const handleClose = () => {
    setOpen(false);
    props.ReinitState();
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        <OpenInNewTwoToneIcon className={classes.icon} />
        Login
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <Paper square>
          <Tabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            onChange={handleChange}
            aria-label="disabled tabs example"
          >
            <LinkTab label="Login" href="/login" {...a11yProps(0)} />
            <LinkTab label="Register" href="/register" {...a11yProps(1)} />
          </Tabs>
          <TabPanel value={value} index={0}>
            <LoginDialog close={handleClose} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <RegisterDialog close={handleClose} />
          </TabPanel>
        </Paper>
      </Dialog>
    </div>
  );
}
export default connect(null, mapDispatchToProps)(LoginTabs);
