import React from "react";
import { usePosition } from "use-position";
import SnackBarComp from "components/shared/SnackbarComp";

export const AskLocation = () => {
  const { latitude, longitude, error } = usePosition(true);

  if (latitude && longitude) {
    localStorage.setItem("latitude", latitude);
    localStorage.setItem("longitude", longitude);
  }

  return (
    <code>
      {error ? (
        <SnackBarComp
          message="You can't access events without location"
          type="error"
        />
      ) : (
        ""
      )}
    </code>
  );
};
