import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Stepper,
  Step,
  StepLabel,
  Button,
  StepContent,
  Paper
} from "@material-ui/core";
import EventsInfos from "./steps/EventsInfos";
import EventDate from "./steps/EventBeginDate";
import EventDetails from "./steps/EventDetails";
import EventDescription from "./steps/EventDescription";
import EventEndDate from "./steps/EventEndingDate";
import { createEvent } from "../../actions/apiFetch/event";
import { connect } from "react-redux";

const useStyles = makeStyles(theme => ({
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

function getSteps() {
  return [
    "Event's main infos",
    "Event's details",
    "Event's Beginning date",
    "Event's Ending",
    "Description and resignation's limit"
  ];
}

function getStepContent(
  step,
  change,
  event,
  activeStep,
  handleBack,
  handleNext,
  steps
) {
  switch (step) {
    case 0:
      return (
        <EventsInfos
          change={change}
          event={event}
          activeStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
          steps={steps}
        />
      );
    case 1:
      return (
        <EventDetails
          change={change}
          event={event}
          activeStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
          steps={steps}
        />
      );
    case 2:
      return (
        <EventDate
          change={change}
          event={event}
          activeStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
          steps={steps}
        />
      );
    case 3:
      return (
        <EventEndDate
          change={change}
          event={event}
          activeStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
          steps={steps}
        />
      );
    case 4:
      return (
        <EventDescription
          change={change}
          event={event}
          activeStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
          steps={steps}
        />
      );

    default:
      return "Unknown step";
  }
}

function VerticalLinearStepper(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [event, setEvent] = React.useState([]);
  const steps = getSteps();

  const handleEventChange = values => {
    event.push(values);
  };
  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleCreateEvent = () => {
    var result = {};
    for (let i = 0; i < event.length; i++) {
      if (typeof event[i] === "object") {
        for (let key in event[i]) {
          result[key] = event[i][key];
        }
      }
    }
    const begin = new Date(result["DateEvent"]);
    const end = new Date(result["endDate"]);
    var Duration = Math.round((end.getTime() - begin.getTime()) / 60000);
    result["Duration"] = Duration;
    result["NumberParticipants"] = parseInt(result["NumberParticipants"]);
    result["Cost"] = parseInt(result["Cost"]);
    delete result["endDate"];
    props.createEvent(result, props.token);
  };

  return (
    <Fragment>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(
                index,
                handleEventChange,
                event,
                activeStep,
                handleBack,
                handleNext,
                steps
              )}
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Button
            onClick={handleCreateEvent}
            className={classes.button}
            variant="contained"
            color="primary"
          >
            Create your event
          </Button>
        </Paper>
      )}
    </Fragment>
  );
}
const mapStateToProps = state => ({
  error: state.common.error,
  token: state.user.token
});

const mapDispatchToProps = {
  createEvent
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VerticalLinearStepper);
