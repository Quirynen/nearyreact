import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}));
export default function NextStep(props) {
  const classes = useStyles();
  const handleEventChange = () => {
    props.change(props.event);
    props.handleNext();
  };
  return (
    <div className={classes.actionsContainer}>
      <div>
        <Button
          disabled={props.activeStep === 0}
          onClick={props.handleBack}
          className={classes.button}
        >
          Back
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleEventChange}
          className={classes.button}
        >
          {props.activeStep === props.steps.length - 1 ? "Finish" : "Next"}
        </Button>
      </div>
    </div>
  );
}
