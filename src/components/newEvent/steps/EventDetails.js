import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  FormControl,
  OutlinedInput,
  InputAdornment,
  InputLabel,
  IconButton,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";
import EuroOutlinedIcon from "@material-ui/icons/EuroOutlined";
import clsx from "clsx";
import NextStep from "./NextStep";
import WorkTwoToneIcon from "@material-ui/icons/WorkTwoTone";
import GoogleMapsPlace from "./GoogleMapsPlace";

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1)
  },
  withoutLabel: {
    marginTop: theme.spacing(3)
  },
  textField: {
    width: "50%"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: "50%",
    maxWidth: "50%",
    textAlign: "center"
  },
  icon: {
    textAlign: "right"
  }
}));
export default function EventDetails(props) {
  const classes = useStyles();

  const [event, setEvent] = React.useState({
    Cost: 0,
    RequiredEquipment: "",
    Place: "",
    isPublic: true
  });
  const handleChange = () => {
    setEvent({ ...event, isPublic: !event.isPublic });
  };
  const adressChange = value => {
    setEvent({ ...event, Place: value });
  };
  return (
    <Fragment>
      <center>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="cost">Event's cost</InputLabel>
          <OutlinedInput
            id="cost"
            onChange={e => setEvent({ ...event, Cost: e.target.value })}
            endAdornment={
              <InputAdornment position="start">
                <EuroOutlinedIcon />
              </InputAdornment>
            }
            labelWidth={100}
            required
            type="number"
          />
        </FormControl>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Required Equipment
          </InputLabel>
          <OutlinedInput
            id="equipment"
            onChange={e =>
              setEvent({ ...event, RequiredEquipment: e.target.value })
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <WorkTwoToneIcon />
                </IconButton>
              </InputAdornment>
            }
            required
            labelWidth={200}
          />
        </FormControl>
        <GoogleMapsPlace adressChange={adressChange} />
        <FormControlLabel
          className={clsx(classes.margin, classes.textField)}
          control={
            <Checkbox
              checked={event.isPublic}
              onChange={handleChange}
              value="isPublic"
              color="primary"
            />
          }
          label="isPublic"
        />
      </center>
      <NextStep
        activeStep={props.activeStep}
        handleBack={props.handleBack}
        handleNext={props.handleNext}
        steps={props.steps}
        change={props.change}
        event={event}
      />
    </Fragment>
  );
}
