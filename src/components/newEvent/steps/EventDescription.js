import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import NextStep from "./NextStep";
import {
  FormControl,
  OutlinedInput,
  InputAdornment,
  InputLabel,
  IconButton,
  TextField,
  MenuItem
} from "@material-ui/core";
import clsx from "clsx";
import DescriptionTwoToneIcon from "@material-ui/icons/DescriptionTwoTone";

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1)
  },
  withoutLabel: {
    marginTop: theme.spacing(3)
  },
  textField: {
    width: "50%"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: "50%",
    maxWidth: "50%",
    textAlign: "center"
  },
  icon: {
    textAlign: "right"
  },
  menu: {
    width: "50%"
  }
}));
const limits = [
  {
    value: 0,
    label: "min",
    nbMin: 0
  },
  {
    value: 30,
    label: "min",
    nbMin: 30
  },
  {
    value: 1,
    label: "h",
    nbMin: 60
  },
  {
    value: 3,
    label: "h",
    nbMin: 180
  },
  {
    value: 6,
    label: "h",
    nbMin: 360
  },
  {
    value: 12,
    label: "h",
    nbMin: 720
  },
  {
    value: 1,
    label: "day",
    nbMin: 1440
  },
  {
    value: 3,
    label: "days",
    nbMin: 4320
  },
  {
    value: 7,
    label: "days",
    nbMin: 10080
  }
];
export default function EventDescription(props) {
  const classes = useStyles();
  const [event, setEvent] = React.useState({
    Description: "",
    CanLeaveUntil: 0
  });
  const handleChange = event => {
    setEvent({ ...event, CanLeaveUntil: event.target.value });
  };
  return (
    <Fragment>
      <center>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="description">Event's Description</InputLabel>
          <OutlinedInput
            id="description"
            multiline
            onChange={e => setEvent({ ...event, Description: e.target.value })}
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <DescriptionTwoToneIcon />
                </IconButton>
              </InputAdornment>
            }
            required
            labelWidth={200}
          />
        </FormControl>
        <TextField
          id="standard-select-resignation"
          select
          label="Resignation's limit"
          className={classes.textField}
          value={event.CanLeaveUntil}
          onChange={handleChange}
          SelectProps={{
            MenuProps: {
              className: classes.menu
            }
          }}
          helperText="Select the resignation's limit to leave your event"
          margin="normal"
          variant="outlined"
        >
          {limits.map(option => (
            <MenuItem key={option.nbMin} value={option.nbMin}>
              {option.value} {option.label}
            </MenuItem>
          ))}
        </TextField>
      </center>
      <NextStep
        activeStep={props.activeStep}
        handleBack={props.handleBack}
        handleNext={props.handleNext}
        steps={props.steps}
        change={props.change}
        event={event}
      />
    </Fragment>
  );
}
