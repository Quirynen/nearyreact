import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  FormControl,
  OutlinedInput,
  InputAdornment,
  InputLabel,
  IconButton,
  Select,
  MenuItem
} from "@material-ui/core";
import clsx from "clsx";
import { GroupOutlined, Edit } from "@material-ui/icons";
import { sports } from "components/shared/SportPossibilities";
import NextStep from "./NextStep";

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1)
  },
  withoutLabel: {
    marginTop: theme.spacing(3)
  },
  textField: {
    width: "50%"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: "50%",
    maxWidth: "50%",
    textAlign: "center"
  },
  icon: {
    right: 0,
    textAlign: "right"
  }
}));
export default function EventsInfos(props) {
  const classes = useStyles();
  const [event, setEvent] = React.useState({
    Name: "",
    NumberParticipants: 0,
    Sport: ""
  });

  return (
    <Fragment>
      <center>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Event's name
          </InputLabel>
          <OutlinedInput
            id="name"
            onChange={e => setEvent({ ...event, Name: e.target.value })}
            endAdornment={
              <InputAdornment position="start">
                <Edit />
              </InputAdornment>
            }
            labelWidth={100}
            required
          />
        </FormControl>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Number of participants
          </InputLabel>
          <OutlinedInput
            id="nbParticipants"
            type="number"
            onChange={e =>
              setEvent({ ...event, NumberParticipants: e.target.value })
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <GroupOutlined />
                </IconButton>
              </InputAdornment>
            }
            required
            labelWidth={200}
          />
        </FormControl>
        <FormControl className={classes.formControl} variant="outlined">
          <InputLabel>Sports</InputLabel>
          <Select
            variant="outlined"
            value={event.Sport}
            labelWidth={50}
            onChange={e => setEvent({ ...event, Sport: e.target.value })}
          >
            {sports.map(item => (
              <MenuItem key={item.text} value={item.text}>
                {item.text}
                <IconButton>{item.icon}</IconButton>
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </center>
      <NextStep
        activeStep={props.activeStep}
        handleBack={props.handleBack}
        handleNext={props.handleNext}
        steps={props.steps}
        change={props.change}
        event={event}
      />
    </Fragment>
  );
}
