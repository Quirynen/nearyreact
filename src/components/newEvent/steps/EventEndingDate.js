import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import NextStep from "./NextStep";
import { Grid } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";

const useStyles = makeStyles(theme => ({
  marginPicker: {
    margin: theme.spacing(3)
  }
}));
export default function EventEndDate(props) {
  const classes = useStyles();
  var today = new Date();
  var tomorrow = new Date();
  tomorrow.setDate(today.getDate() + 1);
  const [event, setEvent] = React.useState({
    endDate: tomorrow
  });

  const handleEndDateChange = date => {
    setEvent({ ...event, endDate: date });
  };

  return (
    <Fragment>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container justify="center">
          <KeyboardDatePicker
            className={classes.marginPicker}
            margin="normal"
            id="date-picker-dialog"
            label="Event's end date"
            format="dd/MM/yyyy"
            value={event.endDate}
            onChange={handleEndDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date"
            }}
          />
          <KeyboardTimePicker
            className={classes.marginPicker}
            margin="normal"
            id="time-picker"
            label="Event's end hour"
            value={event.endDate}
            onChange={handleEndDateChange}
            KeyboardButtonProps={{
              "aria-label": "change time"
            }}
          />
        </Grid>
      </MuiPickersUtilsProvider>
      <NextStep
        activeStep={props.activeStep}
        handleBack={props.handleBack}
        handleNext={props.handleNext}
        steps={props.steps}
        change={props.change}
        event={event}
      />
    </Fragment>
  );
}
