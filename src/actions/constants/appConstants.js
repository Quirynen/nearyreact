// Is smth Loading?
export const IS_LOADING = "IS_LOADING";
export const FINISHED_LOADING = "FINISHED_LOADING";

// Reinit State
export const REINIT_STATE = "REINIT_STATE";

// Base URL for the calls

export const BASEURLUSER_SERVICE = "http://localhost:8051/";
export const BASEURLEVENT_SERVICE = "http://localhost:8050/";

// AN API CALL HAS FAILED

export const ERROR_FETCH = "ERROR_FETCH";
