// User login
export const IS_LOGIN_SUCCESS = "IS_LOGIN_SUCCESS";
export const LOGOUT_USER = "LOGOUT_USER";
export const IS_LOGIN_FAILED = "IS_LOGIN_FAILED";

// User SignUp

export const IS_SIGNUP_SUCCESS = "IS_SIGNUP_SUCCESS";
export const IS_SIGNUP_FAILED = "IS_SIGNUP_FAILED";
