import {
  IS_LOADING,
  BASEURLEVENT_SERVICE,
  FINISHED_LOADING,
  ERROR_FETCH
} from "../constants/appConstants";
import axios from "axios";

export const createEvent = (event, token) => dispatch => {
  dispatch({ type: IS_LOADING });
  axios
    .post(BASEURLEVENT_SERVICE + "api/Events", event, {
      headers: { Authorization: token }
    })
    .then(function(response) {
      dispatch({ type: FINISHED_LOADING, response });
      console.log(response);
    })
    .catch(function(error) {
      const payload = {
        message: error.data
      };
      dispatch({ type: ERROR_FETCH, payload });
      console.log(error);
    });
};
export const getEvents = (token, nbRows) => dispatch => {
  dispatch({ type: IS_LOADING });
  axios
    .get(BASEURLEVENT_SERVICE + "api/Events/getEvents/" + nbRows, {
      headers: {
        Authorization: token,
        latitude: localStorage.getItem("latitude"),
        longitude: localStorage.getItem("longitude"),
        km: localStorage.getItem("km"),
        sports: localStorage.getItem("sports").replace(/[\[\]\,\"]+/g, "")
      }
    })
    .then(function(response) {
      dispatch({ type: FINISHED_LOADING });
      console.log(response);
      return response;
    })
    .catch(function(error) {
      const payload = {
        message: error.data
      };
      dispatch({ type: ERROR_FETCH, payload });
      console.log(error);
    });
};
