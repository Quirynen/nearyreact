import {
  LOGOUT_USER,
  IS_LOGIN_SUCCESS,
  IS_LOGIN_FAILED,
  IS_SIGNUP_SUCCESS,
  IS_SIGNUP_FAILED
} from "../constants/userActionsTypes";
import {
  IS_LOADING,
  BASEURLUSER_SERVICE,
  REINIT_STATE,
  FINISHED_LOADING
} from "../constants/appConstants";
import axios from "axios";

export const logoutUser = () => dispatch => {
  dispatch({ type: LOGOUT_USER });
};
export const ReinitState = () => dispatch => {
  dispatch({ type: REINIT_STATE });
};
export const storeUserInfos = token => dispatch => {
  const payload = {
    user: {
      username: ""
    },
    token: "Bearer " + token
  };
  dispatch({ type: IS_LOGIN_SUCCESS, payload });
};

export const loginUser = (username, password) => dispatch => {
  dispatch({ type: IS_LOADING });
  axios
    .post(BASEURLUSER_SERVICE + "api/Authenticate", { username, password })
    .then(function(response) {
      const payload = {
        user: {
          username: username
        },
        token: "Bearer " + response.data
      };
      dispatch({ type: IS_LOGIN_SUCCESS, payload });
      dispatch({ type: FINISHED_LOADING });
      console.log(payload);
    })
    .catch(function(error) {
      const payload = {
        message: error.response.data
      };
      dispatch({ type: IS_LOGIN_FAILED, payload });
      dispatch({ type: FINISHED_LOADING });
      console.log(error);
    });
};

export const signUpUser = (username, password, mail) => dispatch => {
  dispatch({ type: IS_LOADING });
  axios
    .post(BASEURLUSER_SERVICE + "api/SignUp", { username, password, mail })
    .then(function(response) {
      dispatch(loginUser(username, password));
      dispatch({ type: IS_SIGNUP_SUCCESS });
    })
    .catch(function(error) {
      const payload = {
        message: error.response.data.message
      };
      dispatch({ type: IS_SIGNUP_FAILED, payload });
    });
};
