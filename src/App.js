import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import store from "store";
import Drawer from "components/shared/MiniDrawer";
import { ThemeProvider } from "@material-ui/styles";
import theme from "theme/theme";
import { CssBaseline } from "@material-ui/core";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <CssBaseline />
        <Router>
          <Drawer />
        </Router>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
