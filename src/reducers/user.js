import {
  IS_LOGIN_SUCCESS,
  LOGOUT_USER,
  IS_LOGIN_FAILED,
  IS_SIGNUP_FAILED,
  IS_SIGNUP_SUCCESS
} from "../actions/constants/userActionsTypes";
import { REINIT_STATE } from "../actions/constants/appConstants";

const initialState = {
  user: {
    username: ""
  },
  token: "",
  // Status user
  isLoggedIn: false,

  isSignUpSuccess: false,

  error: "",

  successMessage: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case IS_LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.user,
        token: action.payload.token
      };
    case LOGOUT_USER:
      return {
        ...state,
        isLoading: false,
        isLoggedIn: false,
        user: null,
        token: ""
      };

    case IS_LOGIN_FAILED:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
        token: "",
        error: action.payload.message
      };
    case IS_SIGNUP_FAILED:
      return {
        ...state,
        isSignUpSuccess: false,
        error: action.payload.message
      };
    case IS_SIGNUP_SUCCESS:
      return {
        ...state,
        isSignUpSuccess: true,
        error: ""
      };
    case REINIT_STATE:
      return {
        isSignUpSuccess: false,
        error: "",
        successMessage: ""
      };
    default:
      return state;
  }
}
