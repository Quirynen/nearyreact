import { combineReducers } from "redux";
import events from "reducers/events";
import user from "reducers/user";
import common from "./common";

export default combineReducers({
  events,
  user,
  common
});
