import {
  IS_LOADING,
  FINISHED_LOADING,
  ERROR_FETCH
} from "../actions/constants/appConstants";

const initialState = {
  isLoading: false,

  error: ""
};
export default function(state = initialState, action) {
  switch (action.type) {
    case IS_LOADING:
      return {
        ...state,
        isLoading: true,
        error: ""
      };
    case FINISHED_LOADING:
      return {
        ...state,
        isLoading: false,
        error: ""
      };
    case ERROR_FETCH:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
}
