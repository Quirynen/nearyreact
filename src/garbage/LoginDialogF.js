import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function FormDialog() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ [event.target.id]: event.target.value });
        console.log(this.state.username);
    }
    return (
        <div >
            
            <Dialog className={classes.container} open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title"> <img src={nearyImg} alt="icon" className={classes.imgIcon} />
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Login to your account.
               </DialogContentText>
                    <TextField
                        id="username"
                        required
                        label="Username"
                        fullWidth
                        margin="normal"
                        onChange={handleChange}
                        autoComplete="new-password" />
                    <TextField
                        margin="normal"
                        required
                        id="password"
                        label="Password"
                        type="password"
                        onChange={handleChange}
                        fullWidth
                        autoComplete="new-password" />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained"
                        className={classes.btnCancel}>
                        Cancel
              </Button>
                    <Button onClick={handleClose} variant="contained" color="primary">
                        Login
              </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}