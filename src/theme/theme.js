import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

let theme = createMuiTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 700,
            md: 980,
            lg: 1280,
            xl: 1980
        }
    },
    typography: {
        fontFamily: [      '"Segoe UI Symbol"',].join(','),
    },
    palette: {
        primary: {
            main: '#1b3148'
        },
        secondary: {
            main: '#bbc1c8'
        },
        accent: {
            main: '#FF0000'
        },
        contrastThreshold: 3,
        type: 'light',
        status: {
            danger: 'red'
        }
    },
    /* overrides: {
       MuiCssBaseline: {
         '@global': {
           body: {
             background: `url(${background}) no-repeat center center fixed`,
             backgroundSize: 'cover',
             '-webkit-background-size': 'cover',
             '-moz-background-size': 'cover',
             '-o-background-size': 'cover'
           },
           '::-webkit-scrollbar': {
             width: 10
           },
           '::-webkit-scrollbar-track': {
             background: 'rgba(2,21,35,.7)'
           },
           '::-webkit-scrollbar-thumb': {
             background: '#455a64'
           },
           '::-webkit-scrollbar-thumb:hover': {
             background: '#6a808b'
           }
         }
       },
       MuiDrawer: {
         paper: {
           background:
             'linear-gradient(180deg, rgba(2,21,35,1) 0%, rgba(37,45,48,1) 51%, rgba(11,49,68,1) 100%)'
         }
       }
     }*/
});

theme = responsiveFontSizes(theme);

export default theme;
